var http = require("http")
var fs = require("fs")
var alert = require("alert")

http.createServer(function(req,res){

    //Readfile
    fs.readFile('hello.txt', function(err,data){
        if(err){
            res.writeHead(404, {'Content-Type':'text/html'})
        res.write("File not found!")
        }else{
            res.writeHead(200, {'Content-Type':'text/html'})
            res.write(data)
        }

        //Create file using appendFile
        fs.appendFile("appendFile.txt", "I am Dragon!", function(err){
            if(err) throw err
            //alert("New file with appendFile")
        })
        
        //Create file using open
        fs.open("open.txt", 'w', function(err){
            if(err) throw err
            //alert("New file with open")
        })

        //Create or Replace file with writeFile
        fs.writeFile("writeFile.txt", "I wrote you!", function(err){
            if(err) throw err
            //alert("New file with writeFile!")
        })

        //Update Files ----> Use appendFile or writeFile

        //Delete files --->unlink
        fs.unlink("open.txt", function(err){
            if(err) throw err
            //alert("Open file has been deleted!")
        })

        //Rename files
        fs.rename("djjango.txt", "django.txt", function(err){
            if(err) throw err
                alert("File renamed!")
        })


        res.end();  
})
}).listen(8080)

