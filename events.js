var fs = require("fs")

//open file event
var rs  = fs.createReadStream("hello.txt");
    rs.on('open', function(){
        console.log("The file is open")

    })

//Events module
var events = require("events");
const { EventEmitter } = require("stream");
var eventEmitter = new events.EventEmitter()


//Create event handler
var myEventHandler = function(){
    console.log("They screaming!")

}

//Assign eventhandler to an event
eventEmitter.on("scream", myEventHandler)

//Fire the scream event
eventEmitter.emit("scream")
